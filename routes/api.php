<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserController@login');

Route::apiResource('user',            'UserController');
Route::apiResource('user/type',       'UserTypeController');
Route::apiResource('farmer',          'FarmerController');
Route::apiResource('property/type',   'PropertyTypeController');
Route::apiResource('property',        'PropertyController');
Route::apiResource('animal/agegroup', 'AgeGroupController');
Route::apiResource('animal',          'AnimalController');
Route::apiResource('transportation',  'TransportationController');
Route::apiResource('gta/purpose',     'GTAPurposeController');
Route::apiResource('gta/type',        'GTATypeController');
Route::apiResource('gta',             'GTAController');

// Extra routes for 'Properties' resource
Route::get('property/{property}/coordinates', 'PropertyController@fetchCoordinates');
Route::put('property/{property}/coordinates', 'PropertyController@updateCoordinates');

// Extra routes for 'GTAs' resource
Route::get   ('gta/{gta_id}/animal', 'GTAController@listAnimals');
Route::post  ('gta/{gta_id}/animal/{animal_id}', 'GTAController@addAnimal');
Route::delete('gta/{gta_id}/animal/{animal_id}', 'GTAController@removeAnimal');
