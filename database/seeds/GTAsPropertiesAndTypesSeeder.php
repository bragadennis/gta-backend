
<?php

use Illuminate\Database\Seeder;

use GTAChain\Models\GTAType;
use GTAChain\Models\GTAPurpose;

class GTAsPropertiesAndTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Bovinos e Bufalos'],
            ['name' => 'Aves com Finalidade de Produção de Carnes, Ovos e Material Genetico'],
            ['name' => 'Animais Aquaticos'],
            ['name' => 'Equideos'],
            ['name' => 'Suideos'],
            ['name' => 'Ovinos e Caprinos']
        ];

        $purposes = [
            ['name' => "Abate"],
            ['name' => "Reprodução"],
            ['name' => "Exposição"],
            ['name' => "Feira"],
            ['name' => "Leilao"],
            ['name' => "Esporte"],
            ['name' => "Aglomeração com finalidade comercial"],
            ['name' => "Aglomeração sem finalidade comercial"],
            ['name' => "Engorda"],
            ['name' => "Abate Sanitario"],
            ['name' => "Exportação"],
            ['name' => "Pesquisa"],
            ['name' => "Produtos Biologicos"],
            ['name' => "Quarentena"],
            ['name' => "Destruição"],
            ['name' => "Atendimento Veterinario"],
            ['name' => "Trabalho"],
            ['name' => "Recria"],
            ['name' => "Cria"],
            ['name' => "Pesagem"],
            ['name' => "Retorno de Frigorifico"],
            ['name' => "Retorno a Origem"],
            ['name' => "Ratitas para Incubatorio, Cria e Recria"],
            ['name' => "Venda em Comercio"],
            ['name' => "Postura"],
            ['name' => "Iniciacao"],
            ['name' => "Industrialização"],
            ['name' => "Incubacao"],
            ['name' => "Abate - Origem:Pesca"],
            ['name' => "Abate - Origem:Agricultura/Criacao"],
            ['name' => "Ornamental - Origem:Pesca"],
            ['name' => "Ornamental - Origem:Criacao"],
            ['name' => "PEAE - Propriedade de Espera de Abate de Equídeos"],
            ['name' => "PFE - Propriedade Fornecedora de Equídeos"],
            ['name' => "Equoterapia"],
            ['name' => "Compania"],
            ['name' => "Recria para reprodução"],
        ];

        foreach( $types as $type )
        {
            $t = new GTAType;

            $t->setName($type['name']);

            $t->save();
        }

        foreach( $purposes as $purpose )
        {
            $p = new GTAPurpose;

            $p->setName($purpose['name']);

            $p->save();
        }
    }
}
