<?php

use Illuminate\Database\Seeder;

use GTAChain\Models\UserType;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Seed database with user types.\n";

        $types = [
            ['name' => 'Admin',              'description' => 'Administrador do Sistema',                               'type' => UserType::TYPES['ADMIN'] ],
            ['name' => 'Superv. Prefeitura', 'description' => 'Supervisor da Prefeitura',                               'type' => UserType::TYPES['CITYHALL_SUPERVISOR'] ],
            ['name' => 'Superv. Estado',     'description' => 'Supervisor do Estado',                                   'type' => UserType::TYPES['STATE_SUPERVISOR'] ],
            ['name' => 'Méd. Veterinário',   'description' => 'Médico Veterinário devidamente credenciado pelo ADAPAR', 'type' => UserType::TYPES['VETERINARY'] ],
            ['name' => 'Produtor Rural',     'description' => 'Productor Rural',                                        'type' => UserType::TYPES['FARMER'] ],
        ];

        foreach( $types as $type )
        {
            $objType = new UserType;

            $objType->setName( $type['name'] );
            $objType->setDescription( $type['description'] );
            $objType->setType( $type['type'] );

            $objType->save();
        }
    }
}
