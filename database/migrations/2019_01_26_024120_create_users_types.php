<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['admin', 'cityhall_supervisor', 'state_supervisor', 'veterinary', 'farmer']);
            $table->string('name')->nullable(); 
            $table->string('description')->nullable(); 
        });

        Schema::table('users', function($table) {
            $table->integer('user_type_id')->unsigned();

            $table->foreign('user_type_id')->references('id')->on('user_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropForeign('users_user_type_id_foreign');
            $table->dropColumn('user_type_id');
        });
        Schema::dropIfExists('user_types');
    }
}
