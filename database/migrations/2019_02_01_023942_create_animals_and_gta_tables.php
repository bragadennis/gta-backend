<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsAndGtaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('age_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bottom_limit');
            $table->integer('upper_limit');
            $table->string('group', 15);
        });

        Schema::create('animals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('age_group_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->integer('amount');
            $table->enum('gender', ['m', 'f', 'n/a']);
            $table->string('species', 100)->nullable();
            $table->string('classification', 150)->nullable();
            $table->string('race')->nullable();

            $table->foreign('age_group_id')->references('id')->on('age_groups')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });

        Schema::create('transportations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_identification');
            $table->string('vehicle_manufacturer')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('plates');
            $table->string('drivers_name');
            $table->string('drivers_licence_number')->nullable();
        });

        Schema::create('gta_purposes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('acronym')->nullable();
        });

        Schema::create('gta_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('acronym')->nullable();
        });

        Schema::create('gtas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('origin_id')->unsigned();
            $table->integer('destination_id')->unsigned();
            $table->integer('transportation_id')->unsigned();
            $table->integer('gta_purpose_id')->unsigned();
            $table->integer('gta_type_id')->unsigned();
            $table->enum('status', ['registered', 'approved', 'in_transit', 'received', 'cancelled', 'warning']);
            $table->timestamps();

            $table->foreign('origin_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('destination_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('transportation_id')->references('id')->on('transportations')->onDelete('cascade');
            $table->foreign('gta_purpose_id')->references('id')->on('gta_purposes')->onDelete('cascade');
            $table->foreign('gta_type_id')->references('id')->on('gta_types')->onDelete('cascade');
        });

        Schema::create('animal_gta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gta_id')->unsigned();
            $table->integer('animal_id')->unsigned();
            $table->integer('quantity');
            $table->boolean('commited')->default(false);

            $table->index(['gta_id', 'animal_id']);
            $table->foreign('gta_id')->references('id')->on('gtas')->onDelete('cascade');
            $table->foreign('animal_id')->references('id')->on('animals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_gta');
        Schema::dropIfExists('gtas');
        Schema::dropIfExists('gta_types');
        Schema::dropIfExists('gta_purposes');
        Schema::dropIfExists('transportations');
        Schema::dropIfExists('animals');
        Schema::dropIfExists('age_groups');   
    }
}
