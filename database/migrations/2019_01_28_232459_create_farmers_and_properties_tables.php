<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmersAndPropertiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('acronym')->unique();
            $table->string('name');
            $table->string('description')->nullable();
        });
        
        Schema::create('farmers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('nickname')->nullable();
            $table->enum('identifier_flag', ['cpf', 'cnpj']);
            $table->string('identifier_number');
            $table->string('full_address');
            $table->string('cell_number')->nullable();
        });
        
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_type_id')->unsigned();
            $table->integer('farmer_id')->unsigned();
            $table->string('cnpj', 13);
            $table->string('full_address');
            $table->string('name')->nullable();
            $table->string('latitude', 30)->nullable();
            $table->string('longitude', 30)->nullable();
            

            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('cascade');
            $table->foreign('farmer_id')->references('id')->on('farmers')->onDelete('cascade');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
        Schema::dropIfExists('farmers');
        Schema::dropIfExists('property_types');
        
    }
}
