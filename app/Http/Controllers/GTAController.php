<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use Exception;

use GTAChain\Models\GTA;
use GTAChain\Models\Animal;
use GTAChain\Models\GTAType;
use GTAChain\Models\Property;
use GTAChain\Models\GTAPurpose;
use GTAChain\Models\Transportation;

class GTAController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $gtas = GTA::all();

            if( $gtas->count() < 1 ) // The list of GTA types is empty
            {
                return $this->addMessage("GTAs", "No GTAs available on the database")
                            ->sendOk();
            }

            $this->addResponse('gtas', $gtas);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gta = new GTA();
        
        try {
            $purpose = GTAPurpose::findOrFail( $request->gta_purpose_id );
            $type    = GTAType::findOrFail( $request->gta_type_id );
            $origin  = Property::findOrFail( $request->origin_id );
            $destination    = Property::findOrFail( $request->destination_id );
            $transportation = Transportation::findOrFail( $request->transportation_id );

            $gta->setOrigin($origin);
            $gta->setDestination($destination);
            $gta->setTransport($transportation);
            $gta->setPurpose($purpose);
            $gta->setType($type);
            $gta->setStatus( $request->status );

            $gta->save();

            $this->addResponse('gta', $gta);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $gta = GTA::find($id);

            $this->addResponse('gta', $gta);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $gta = GTA::findOrFail($id);
            if( $request->has('origin_id') )
            {
                $origin = Property::find($request->origin_id);

                if( $origin )
                    $gta->setOrigin($origin);
            }

            if( $request->has('destination_id') )
            {
                $destination = Property::find($request->destination_id);

                if( $destination )
                    $gta->setDestination($destination);
            }

            if( $request->has('transportation_id') )
            {
                $transportation = Transportation::find($request->transportation_id);

                if( $transportation )
                    $gta->setTransport($transportation);
            }

            if( $request->has('gta_purpose_id') )
            {
                $purpose = GTAPurpose::find($request->gta_purpose_id);

                if( $purpose )
                    $gta->setPurpose($purpose);
            }

            if( $request->has('gta_type_id') )
            {
                $type = GTAType::find($request->gta_type_id);

                if( $type )
                    $gta->setType($type);
            }

            if( $request->has('status') )
                $gta->setStatus( $request->status );
            
            $gta->save();

            $this->addResponse('gta', $gta);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $gta = GTA::find($id);
    
            if( $gta->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: GTA; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }

    public function addAnimal(Request $request, $gta_id, $animal_id)
    {
        try {
            if ( ! $request->has('amount') )
                return $this->sendFail(new Exception('Amount is a mandatory field'), 500, 'Unable to find "amount" field in request');

            $gta = GTA::findOrFail($gta_id);
            $animal = Animal::findOrFail($animal_id);

            $gta->attachAnimal( $animal, $request->amount );

            $gta->save();

            $this->addResponse('gta',    $gta);
            $this->addResponse('animal', $animal);
            $this->addResponse('pivot',  $gta->getAnimals());

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    public function listAnimals($gta_id)
    {
        try {
            $gta = GTA::findOrFail($gta_id);

            $this->addResponse('gta',    $gta);
            $this->addResponse('animals',  $gta->getAnimals());

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    public function removeAnimal($gta_id, $animal_id)
    {
        try {
            $gta = GTA::findOrFail($gta_id);
            $animal = Animal::findOrFail($animal_id);
    
            if( $gta->detachAnimal( $animal ) )
                return $this->sendOk();
            else 
                return $this->sendFail(new Exception('Operation "detachAnimal" went wrong'), 500, "Unable to delete resource. Resourse: GTA; Identifier => GTA: $gta_id; ANIMAL: $animal_id");
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }
}
