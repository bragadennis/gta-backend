<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\GTA;
use GTAChain\Models\Transportation;
use Exception;

class TransportationController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $transportions = Transportation::all();

            if( $transportions->count() < 1 ) // The list of GTA types is empty
            {
                return $this->addMessage("transportation", "No transportation available on the database")
                            ->sendOk();
            }

            $this->addResponse('transportations', $transportions);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transport = new Transportation();
        
        try {
            $transport->setCompanyName($request->company_name);
            $transport->setCompanyIdentification($request->company_identification);
            $transport->setPlates($request->plates);
            $transport->setDriversName($request->drivers_name);

            if( $request->has('vehicle_manufacturer') )
                $transport->setVehicleManufacturer($request->vehicle_manufacturer);
        
            if( $request->has('vehicle_color') )
                $transport->setVehicleColor($request->vehicle_color);
        
            if( $request->has('drivers_licence_number') )
                $transport->setDriversLicenceNumber($request->drivers_licence_number);
        
            $transport->save();

            $this->addResponse('transportation', $transport);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $transport = Transportation::find($id);

            $this->addResponse('transportation', $transport);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $transport = Transportation::find($id);
            
            if( $request->has('company_name') )
                $transport->setCompanyName($request->company_name);

            if( $request->has('company_identification') )
                $transport->setCompanyIdentification($request->company_identification);

            if( $request->has('vehicle_manufacturer') )
                $transport->setVehicleManufacturer($request->vehicle_manufacturer);
        
            if( $request->has('vehicle_color') )
                $transport->setVehicleColor($request->vehicle_color);

            if( $request->has('plates') )
                $transport->setPlates($request->plates);

            if( $request->has('drivers_name') )
                $transport->setDriversName($request->drivers_name);
        
            if( $request->has('drivers_licence_number') )
                $transport->setDriversLicenceNumber($request->drivers_licence_number);
            
            $transport->save();

            $this->addResponse('transportation', $transport);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $transport = Transportation::find($id);
    
            if( $transport->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: Transportation; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
