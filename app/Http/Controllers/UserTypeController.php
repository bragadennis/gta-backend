<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\UserType;
use Exception;

class UserTypeController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $types = UserType::all();

            if( $types->count() < 1 ) // The list of users is empty
            {
                return $this->addMessage("types", "No types available on the database")
                            ->sendOk();
            }

            $this->addResponse('types', $types);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incoming_type = UserType::TYPES[$request->type];

        if( ! $incoming_type )
            return $this->addMessage("types", "No types on database matches the incoming requested type")
                        ->sendOk();

        $type = new UserType();

        try {
            $type->setName($request->name);
            $type->setDescription($request->description);
            $type->setType($request->type);

            $type->save();

            $this->addResponse('type', $type);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = UserType::find($id);

            $this->addResponse('type', $type);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $type = UserType::find($id);

            if( $request->has('name') )
                $type->setName($request->name);
    
            if($request->has('description'))
                $type->setDescription($request->description);
            
            if($request->has('type') AND isset(UserType::TYPES[$request->type]))
                $type->setEmail(UserType::TYPES[$request->type]);

            $type->save();

            $this->addResponse('type', $type);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = UserType::find($id);
    
            if( $type->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: User Type; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
