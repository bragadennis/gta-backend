<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use GTAChain\Models\User;
use GTAChain\Models\UserType;
use Exception;

class UserController extends ApiBaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            $user = Auth::user(); 

            $this->addResponse('token', $user->createToken('GTAChain')->accessToken);
            $this->addResponse('user', $user);

            return $this->sendOk();
        }
        else {
            return $this->addMessage("login", 'Login credentials doesn\'t match our records!' )->sendOk();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::all();

            if( $users->count() < 1 ) // The list of users is empty
            {
                $this->addMessage("users", "No users available on the database")
                     ->sendOk();
            }

            $this->addResponse('users', $users);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();

        try {
            $user->setPassword($request->password, $request->password_confirmation);

            $type = UserType::find($request->type_id);
    
            $user->setName($request->name);
            $user->setEmail($request->email);
            $user->setType($type);

            $user->save();

            $this->addResponse('user', $user);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::find($id);

            $this->addResponse('user', $user);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);

            if($request->has('password') && $request->has('password_confirmation'))
                $user->setPassword($request->password, $request->password_confirmation);
    
            if($request->has('name'))
                $user->setName($request->name);
            
            if($request->has('email'))
                $user->setEmail($request->email);

            $user->save();

            $this->addResponse('user', $user);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
    
            if( $user->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: User; Identifier: $company_id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
