<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\Animal;
use GTAChain\Models\AgeGroup;
use GTAChain\Models\Property;
use Exception;

class AnimalController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $animals = Animal::all();

            if( $animals->count() < 1 ) // The list of users is empty
            {
                return $this->addMessage("animals", "No animals available on the database")
                            ->sendOk();
            }

            $this->addResponse('animals', $animals);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = AgeGroup::find( $request->input('age_group_id') );

        if( ! $request->has('age_group_id') OR ! $group )
            return $this->addMessage("age_group", "No age group available for the provided ID")
                        ->sendOk();

        $property = Property::find( $request->input('property_id') );

        if( ! $request->has('property_id') OR ! $property )
            return $this->addMessage("property", "No property available for the provided ID")
                        ->sendOk();

        $animal = new Animal();

        try {
            $animal->setAmount($request->amount);
            $animal->setGender($request->gender);
            
            if( $request->has('species') )
                $animal->setSpecies($request->species);

            if( $request->has('classification') )
                $animal->setClassification($request->classification);

            if( $request->has('race') )
                $animal->setRace($request->race);

            if( $group )
                $animal->setAgeGroup($group);

            if( $property )
                $animal->setProperty($property);

            $animal->save();

            $this->addResponse('animal', $animal);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $animal = Animal::find($id);

            $this->addResponse('animal', $animal);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $animal = Animal::find($id);

            if( $request->has('amount') )
                $animal->setAmount($request->amount);

            if( $request->has('gender') )
                $animal->setGender($request->gender);
            
            if( $request->has('species') )
                $animal->setSpecies($request->species);

            if( $request->has('classification') )
                $animal->setClassification($request->classification);

            if( $request->has('race') )
                $animal->setRace($request->race);

            if( $request->has('age_group_id') ) 
            {
                $group = AgeGroup::find( $request->age_group_id );

                if( $group )
                    $animal->setAgeGroup($group);
                else 
                    $this->addMessage("age_group", "No age group available for the provided ID");
            }

            $animal->save();

            $this->addResponse('animal', $animal);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $animal = Animal::find($id);
    
            if( $animal->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: Animal; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
