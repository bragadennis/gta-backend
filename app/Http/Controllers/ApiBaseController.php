<?php

namespace GTAChain\Http\Controllers;

use GTAChain\Http\Controllers\Controller;
use Exception;

class ApiBaseController extends Controller
{
    const STATUSES_KEYS = ['OK' => 'ok', 'WITH_ERRORS' => 'with_errors', 'CRASHED' => 'crashed'];
    const STATUS_KEY  = "status";
    const BODY_KEY    = "body";
    const MESSAGE_KEY = "message";

    /**
     * Holds the body response for all requests.
     *
     * @var array $response
     */
    private $response = [self::STATUS_KEY => "", self::BODY_KEY => [], self::MESSAGE_KEY => []];

    private $http_code = 200;

    /**
     * To be called to responde to 'everything worked' calls.
     *
     * @param boolean $partially_ok (optional)  - Parameter to inform if the request was processed accordinly, or if there's some glitch.
     * @return Redirect to $this->send()
     */
    public function sendOk()
    {
        if( empty($this->response[self::MESSAGE_KEY]) )
            $this->setStatus(self::STATUSES_KEYS['OK']);
        else
            $this->setStatus(self::STATUSES_KEYS['WITH_ERRORS']);

        return $this->send();
    }

    /**
     * To be called to response to 'something went wrong' calls.
     *
     * @param Exception $exception  - Represents the exception raised on the controller, or at any files called by it.
     * @param Int $http_error_code  - An HTTP code response to be sent on the header response for the particular error.
     * @param String $error_message  - A human readable messagem that may help give a clue about what wnet wrong.
     * @return Redirects to $this->send();
     */
    public function sendFail(Exception $exception, int $http_code_error, $error_message = null )
    {
        $this->setStatus(self::STATUSES_KEYS['CRASHED']);

        if( $error_message != null AND ! empty($error_message) )
            $this->addMessage('human_readable', $error_message);

        $this->addDebugMessage($exception);

        $this->http_code = $http_code_error;

        return $this->send();
    }
    
    /**
     * Adds a object to the body under certain key.
     *
     * @param [String] $key  - Represent the key for which the value should be stored and later accessed.
     * @param [Object] $object  - Represent the data to be inserted into the body response.
     * @return this
     */
    public function addResponse(String $key, $object)
    {
        $this->response[self::BODY_KEY]["$key"] = $object;

        return $this;
    }
    
    /**
     * Adds a message to the body of the response.
     *
     * @access Public
     * @param  String $key  - Represents the key to the information being stored (non-spaced string).
     * @param  $message  - Message to be stored under the given key.
     * @return this
     */
    public function addMessage(String $key, $message)
    {
        $this->response[self::MESSAGE_KEY][$key] = $message;

        return $this;
    }

    ####
    #   PRIVATE METHODS AREA
    ####

    /**
     * Undocumented function
     *
     * @param [type] $status
     * @return void
     */
    private function setStatus($status)
    {
        $this->response[self::STATUS_KEY] = $status;

        return $this;
    }

    /**
     * Adds a debug message based on the exception provided.
     *
     * @param Exception $exception  - Represents the exception rised on the controller, or by any code called by it.
     * @return this
     */
    private function addDebugMessage( Exception $exception )
    {
        $this->addMessage('debug_message', [
            'file' => $exception->getFile(), 
            'line' => $exception->getLine(), 
            'message' => $exception->getMessage(), 
            'trace' => array_slice($exception->getTrace(), 0, 15)  
        ]);

        return $this;
    }

    /**
     * This method should should assemble and return an JSON as body of an HTTP request.
     *
     * @return [HTTP Response] JSON containing all the informations regarding the request.
     */
    private function send()
    {
        return response()->json($this->response);
    }
}
