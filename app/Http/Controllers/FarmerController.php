<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\Farmer;
use Exception;

class FarmerController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $farmers = Farmer::all();

            if( $farmers->count() < 1 ) // The list of users is empty
            {
                return $this->addMessage("farmers", "No farmers available on the database")
                            ->sendOk();
            }

            $this->addResponse('farmers', $farmers);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $farmer = new Farmer();

        try {
            if( $request->identifier_flag != 'cnpj' && $request->identifier_flag != 'cpf' )
                return $this->sendFail(new \Exception('Invalid flag for identifier'), 500, "Invalid value for the 'identifier_flag' field. Must be either 'cnpj' or 'cpf'. Resourse: Farmer; Method: store;");

            $farmer->setIdentifierFlag($request->identifier_flag);

            $farmer->setFullname($request->fullname);
            $farmer->setIdentifierNumber($request->identifier_number);
            $farmer->setFullAddress($request->full_address);

            if( $request->has('nickname') )
                $farmer->setNickname($request->nickname);

            if( $request->has('cell_number') )
                $farmer->setCellNumber($request->cell_number);

            $farmer->save();

            $this->addResponse('farmer', $farmer);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $farmer = Farmer::find($id);

            $this->addResponse('farmer', $farmer);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $farmer = Farmer::find($id);

            if( $request->has('identifier_flag') && ($request->identifier_flag != 'cnpj' && $request->identifier_flag != 'cpf') )
                return $this->sendFail(new \Exception('Invalid flag for identifier'), 500, "Invalid value for the 'identifier_flag' field. Must be either 'cnpj' or 'cpf'. Resourse: Farmer; Method: store;");

            if( $request->has('identifier_flag') )
                $farmer->setIdentifierFlag($request->identifier_flag);

            if( $request->has('fullname') )
                $farmer->setFullname($request->fullname);

            if( $request->has('identifier_number') )
                $farmer->setIdentifierNumber($request->identifier_number);

            if( $request->has('full_address') )
                $farmer->setFullAddress($request->full_address);

            if( $request->has('nickname') )
                $farmer->setNickname($request->nickname);

            if( $request->has('cell_number') )
                $farmer->setCellNumber($request->cell_number);

            $farmer->save();

            $this->addResponse('farmer', $farmer);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $farmer = Farmer::find($id);
    
            if( $farmer->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail(new Exception('Unable to delete resource'), 500, "Unable to delete resource. Resourse: Farmer; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
