<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\GTA;
use GTAChain\Models\GTAPurpose;
use Exception;

class GTAPurposeController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $purposes = GTAPurpose::all();

            if( $purposes->count() < 1 ) // The list of purposes is empty
            {
                return $this->addMessage("GTA_purpose", "No GTA Purpose available on the database")
                            ->sendOk();
            }

            $this->addResponse('purposes', $purposes);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purpose = new GTAPurpose();
        
        try {
            $purpose->setName($request->name);
            
            if( $request->has('acronym') )
                $purpose->setAcronym($request->acronym);
            
            $purpose->save();

            $this->addResponse('GTA_purpose', $purpose);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $purpose = GTAPurpose::find($id);

            $this->addResponse('GTA_purpose', $purpose);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try {
            $purpose = GTAPurpose::find($id);
            if( $request->has('name') )
                $purpose->setName($request->name);
            
            if( $request->has('acronym') )
                $purpose->setAcronym($request->acronym);
            
            $purpose->save();

            $this->addResponse('GTA_purpose', $purpose);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $purpose = GTAPurpose::find($id);
    
            if( $purpose->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: GTAPurpose; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
