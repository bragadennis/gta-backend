<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\GTAType;
use Exception;

class GTATypeController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $types = GTAType::all();

            if( $types->count() < 1 ) // The list of GTA types is empty
            {
                return $this->addMessage("GTA_purpose", "No GTA Type available on the database")
                            ->sendOk();
            }

            $this->addResponse('types', $types);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new GTAType();
        
        try {
            $type->setName($request->name);
            
            if( $request->has('acronym') )
                $type->setAcronym($request->acronym);
            
            $type->save();

            $this->addResponse('GTA_type', $type);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = GTAType::find($id);

            $this->addResponse('GTA_type', $type);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try {
            $type = GTAType::find($id);
            if( $request->has('name') )
                $type->setName($request->name);

            if( $request->has('acronym') )
                $type->setAcronym($request->acronym);
            
            $type->save();

            $this->addResponse('GTA_type', $type);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = GTAType::find($id);
    
            if( $type->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: GTAType; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
