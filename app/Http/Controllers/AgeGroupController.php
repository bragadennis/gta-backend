<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\AgeGroup;

use Exception;

class AgeGroupController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $groups = AgeGroup::all();

            if( $groups->count() < 1 ) // The list of users is empty
            {
                return $this->addMessage("groups", "No age groups available on the database")
                            ->sendOk();
            }

            $this->addResponse('age_groups', $groups);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new AgeGroup();

        try {
            $group->setGroup( $request->group );
            $group->setBottomLimit( $request->bottom_limit );
            $group->setUpperLimit( $request->upper_limit );

            $group->save();

            $this->addResponse('age_group', $group);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $group = AgeGroup::find($id);

            $this->addResponse('age_group', $group);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $group = AgeGroup::find($id);

            if( $request->has('group') )
                $group->setGroup($request->group);

            if( $request->has('bottom_limit') )
                $group->setBottomLimit($request->bottom_limit);
            
            if( $request->has('upper_limit') )
                $group->setUpperLimit($request->upper_limit);

            $group->save();

            $this->addResponse('age_group', $group);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $group = AgeGroup::find($id);
    
            if( $group->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: Age Group; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
