<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\Farmer;
use GTAChain\Models\Property;
use GTAChain\Models\PropertyType;
use Exception;

class PropertyController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $properties = Property::all();

            if( $properties->count() < 1 ) // The list of properties is empty
            {
                return $this->addMessage("types", "No types available on the database")
                            ->sendOk();
            }

            $this->addResponse('properties', $properties);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $property = new Property();
        $farmer   = Farmer::find($request->get('farmer_id'));
        $property_type = PropertyType::find($request->get('type_property_id'));

        try {
            $property->setCNPJ($request->cnpj);
            $property->setFullAddress($request->full_address);

            if( $request->has('name') )
                $property->setName($request->name);
            
            if( $request->has('latitude') )
                $property->setLatitude($request->latitude);
            
            if( $request->has('longitude') )
                $property->setLongitude($request->longitude);

            $property->setFarmer($farmer);
            $property->setType($property_type);

            $property->save();

            $this->addResponse('property', $property);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $property = Property::find($id);

            $this->addResponse('property', $property);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $property = Property::find($id);

            if( $request->has('cnpj') )
                $property->setCNPJ($request->cnpj);

            if( $request->has('full_address') )
                $property->setFullAddress($request->full_address);

            if( $request->has('name') )
                $property->setName($request->name);
            
            if( $request->has('latitude') )
                $property->setLatitude($request->latitude);
            
            if( $request->has('longitude') )
                $property->setLongitude($request->longitude);

            if( $request->has('farmer_id') ) 
            {
                $farmer = Farmer::find($request->farmer_id);

                if( ! empty($farmer) )
                    $property->setFarmer($farmer);
            }

            if( $request->has('property_type_id') )
            {
                $property_type = PropertyType::find($request->property_type_id);

                if( ! empty($property_type) )
                    $property->setType($property_type);
            }

            $property->save();

            $this->addResponse('property', $property);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $property = Property::find($id);
    
            if( $property->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: Property Controler; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }

    public function fetchCoordinates($property_id) 
    {
        try {
            $property = Property::find($property_id);

            $this->addResponse('coordinates', $property->getCoordinates() );
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    public function updateCoordinates(Request $request, $property_id) 
    {
        try {
            $property = Property::find($property_id);

            if( $request->has('coordinates') AND ! empty($request->coordinates) )
                foreach($request->coordinates as $coord)
                    $property->addCoordinates((Integer) $coord['order'], (String) $coord['latitude'], (String) $coord['longitude']);

            $this->addResponse('coordinates', $property->getCoordinates() );
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

}
