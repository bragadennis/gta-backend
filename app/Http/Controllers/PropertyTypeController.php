<?php

namespace GTAChain\Http\Controllers;

use Illuminate\Http\Request;
use GTAChain\Models\PropertyType;
use Exception;

class PropertyTypeController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $types = PropertyType::all();

            if( $types->count() < 1 ) // The list of users is empty
            {
                return $this->addMessage("property_types", "No property types available on the database")
                            ->sendOk();
            }

            $this->addResponse('types', $types);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new PropertyType();

        try {
            $type->setName($request->name);
            $type->setAcronym($request->acronym);
            
            if( $request->has('description') )
                $type->setDescription($request->description);

            $type->save();

            $this->addResponse('type', $type);

            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $type = PropertyType::find($id);

            $this->addResponse('property_type', $type);
            return $this->sendOk();
        } catch(\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $type = PropertyType::find($id);

            if( $request->has('name') )
                $type->setName($request->name);
    
            if($request->has('description'))
                $type->setDescription($request->description);
            
            if($request->has('acronym'))
                $type->setAcronym($request->acronym);

            $type->save();

            $this->addResponse('type', $type);
            return $this->sendOk();
        } catch (\Exception $ex) {
            return $this->sendFail($ex, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = PropertyType::find($id);
    
            if( $type->delete() )
                return $this->sendOk();
            else 
                return $this->sendFail($ex, 500, "Unable to delete resource. Resourse: User Type; Identifier: $id;");
        } catch (\Exception $ex) {
            return $this->sendFail($ex);
        }
    }
}
