<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\Property;

class PropertyCoordinate extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    // protected $table = "user_types";

    ####
    #   Relationship Definition Area
    ####

    public function property() 
    {
        return $this->belongsTo(Property::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getProperty() : Property
    {
        return $this->property;
    }

    public function setProperty(Property $property) : PropertyCoordinate
    {
        $this->property()->associate($property);

        return $this;
    }

    public function getOrder() 
    {
        return $this->order;
    }

    public function setOrder(String $order) : PropertyCoordinate
    {
        $this->order = $order;

        return $this;
    }

    public function getLatitude() 
    {
        return $this->latitude;
    }

    public function setLatitude(String $latitude) : PropertyCoordinate
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude() 
    {
        return $this->longitude;
    }

    public function setLongitude(String $longitude) : PropertyCoordinate
    {
        $this->longitude = $longitude;

        return $this;
    }

    ####
    #   Static Method Area
    ####

    // TO-DO: Document
    static public function create(Property $property, int $order, String $latitude, String $longitude)
    {
        $instance = self::where('property_id' , '=', $property->id)->where('order', '=', $order)->first();
        if( ! $instance )
            $instance = new self;

        $instance->setProperty( $property );
        $instance->setOrder( $order );
        $instance->setLatitude( $latitude );
        $instance->setLongitude( $longitude );

        $instance->save(); 

        return $instance;
    }
}
