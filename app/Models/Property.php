<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\PropertyType;
use GTAChain\Models\PropertyCoordinate;
use GTAChain\Models\Farmer;

class Property extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    ####
    #   Relationship Definition Area
    ####

    public function type() 
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function farmer()
    {
        return $this->belongsTo(Farmer::class);
    }

    public function coordinates() 
    {
        return $this->hasMany(PropertyCoordinate::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getType() : PropertyType
    {
        return $this->type()->first();
    }

    public function setType(PropertyType $type) : Property
    {
        $this->type()->associate($type);

        return $this;
    }

    public function getFarmer() : Farmer
    {
        return $this->farmer()->first();
    }

    public function setFarmer(Farmer $farmer) : Property
    {
        $this->farmer()->associate( $farmer );

        return $this;
    }

    public function getCoordinates() 
    {
        return $this->coordinates()->get();
    }

    public function addCoordinates(int $order, String $latitude, String $longitude) : Property
    {
        $coordinates = PropertyCoordinates::createOrUpdate($this, $order, $latitude, $longitude);

        $coordinates->property()->associate( $this );

        return $this;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName(String $name) : Property
    {
        $this->name = $name;

        return $this;
    }

    public function getCNPJ() 
    {
        return $this->cnpj;
    }

    public function setCNPJ(String $cnpj) : Property
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function getFullAddress() 
    {
        return $this->full_address;
    }

    public function setFullAddress(String $full_address) : Property
    {
        $this->full_address = $full_address;

        return $this;
    }

    public function getLatitude() 
    {
        return $this->latitude;
    }

    public function setLatitude(String $latitude) : Property
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude() 
    {
        return $this->longitude;
    }

    public function setLongitude(String $longitude) : Property
    {
        $this->longitude = $longitude;

        return $this;
    }

    ####
    #   Binding functions
    ####

    public function getPropertyCoordinates()
    {
        return [
            'lat' => $this->getLatitude(), 
            'lon' => $this->getLongitude()
        ];
    }
}
