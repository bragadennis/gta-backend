<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\User;
use GTAChain\Models\BaseModel;

class UserType extends BaseModel
{
    const TYPES = [
        'ADMIN' => 'admin', 
        'CITYHALL_SUPERVISOR' => 'cityhall_supervisor', 
        'STATE_SUPERVISOR' => 'state_supervisor', 
        'VETERINARY' => 'veterinary', 
        'FARMER' => 'farmer', 
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = "user_types";

    ####
    #   Relationship Definition Area
    ####

    public function users() 
    {
        return $this->hasMany(User::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getUsers() 
    {
        return $this->users()->get();
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName(String $name) : UserType
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription() 
    {
        return $this->description;
    }

    public function setDescription(String $description) : UserType
    {
        $this->description = $description;

        return $this;
    }

    public function getType() 
    {
        return $this->type;
    }

    public function setType(String $type) : UserType
    {
        $this->type = $type;

        return $this;
    }
}
