<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\Property;

class Farmer extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    ####
    #   Relationship Definition Area
    ####

    public function properties() 
    {
        return $this->hasMany(Property::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getProperties() 
    {
        return $this->properties()->get();
    }

    public function getFullname() 
    {
        return $this->fullname;
    }

    public function setFullname(String $fullname) : Farmer
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getNickname() 
    {
        return $this->nickname;
    }

    public function setNickname(String $nickname) : Farmer
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getIdentifierFlag() 
    {
        return $this->identifier_flag;
    }

    public function setIdentifierFlag(String $identifier_flag) : Farmer
    {
        if( $identifier_flag !== 'cpf' AND $identifier_flag !== 'cnpj' )
            throw new Exception('Invalid option for flag');

        $this->identifier_flag = $identifier_flag;

        return $this;
    }

    public function getIdentifierNumber() 
    {
        return $this->identifier_number;
    }

    public function setIdentifierNumber(String $identifier_number) : Farmer
    {
        $this->identifier_number = $identifier_number;

        return $this;
    }

    public function getFullAddress() 
    {
        return $this->full_address;
    }

    public function setFullAddress(String $full_address) : Farmer
    {
        $this->full_address = $full_address;

        return $this;
    }

    public function getCellNumber() 
    {
        return $this->cell_number;
    }

    public function setCellNumber(String $cell_number) : Farmer
    {
        $this->cell_number = $cell_number;

        return $this;
    }
}
