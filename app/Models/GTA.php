<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\Transportation;
use GTAChain\Models\Property;
use GTAChain\Models\Animal;

class GTA extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $table = "gtas";

    ####
    #   Relationship Definition Area
    ####

    public function origin() 
    {
        return $this->belongsTo(Property::class);
    }

    public function destination() 
    {
        return $this->belongsTo(Property::class);
    }

    public function transport() 
    {
        return $this->belongsTo(Transportation::class, 'transportation_id');
    }

    public function type() 
    {
        return $this->belongsTo(GTAType::class, 'gta_type_id');
    }

    public function purpose() 
    {
        return $this->belongsTo(GTAPurpose::class, 'gta_purpose_id');
    }

    public function animals()
    {
        return $this->belongsToMany(Animal::class, 'animal_gta', 'gta_id', 'animal_id')->withPivot('quantity');
    }

    ####
    #   Getters/Setters Area
    ####

    public function getOrigin() : Property
    {
        return $this->origin;
    }

    public function setOrigin( Property $origin) : GTA
    {
        $this->origin()->associate( $origin );

        return $this;
    }
    
    public function getDestination() : Property
    {
        return $this->destination;
    }

    public function setDestination( Property $destination) : GTA
    {
        $this->destination()->associate( $destination );

        return $this;
    }
    
    public function getTransport() : Transportation
    {
        return $this->transport;
    }

    public function setTransport( Transportation $transport) : GTA
    {
        $this->transport()->associate( $transport );

        return $this;
    }
    
    public function getType() : GTAType
    {
        return $this->type;
    }

    public function setType( GTAType $type) : GTA
    {
        $this->type()->associate( $type );

        return $this;
    }
    
    public function getPurpose() : GTAPurpose
    {
        return $this->purpose;
    }

    public function setPurpose( GTAPurpose $purpose ) : GTA
    {
        $this->purpose()->associate( $purpose );

        return $this;
    }

    public function getAnimals()
    {
        return $this->animals()->get();
    }

    public function attachAnimal( Animal $animal, int $amount ) : GTA
    {
        if( $amount < 1 )
            throw new Exception("Amount of animals can't be minor than one!");

        // TO-DO: Verify animal availability.

        $this->animals()->attach( $animal, ['quantity' => $amount]);

        return $this;
    }

    public function detachAnimal( Animal $animal )
    {
        // TO-DO: Verify permission for the operation
        return $this->animals()->detach( $animal );
    }

    public function getStatus() 
    {
        return $this->status;
    }

    public function setStatus(String $status) : GTA
    {
        $this->status = $status;

        return $this;
    }
}
