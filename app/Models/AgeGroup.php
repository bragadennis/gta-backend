<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\Animal;

class AgeGroup extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    // protected $table = "user_types";

    ####
    #   Relationship Definition Area
    ####

    public function animals() 
    {
        return $this->hasMany(Animal::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getAnimals() 
    {
        return $this->animals()->get();
    }

    public function getGroup() 
    {
        return $this->group;
    }

    public function setGroup(String $group) : AgeGroup
    {
        $this->group = $group;

        return $this;
    }

    public function getBottomLimit() 
    {
        return $this->bottom_limit;
    }

    public function setBottomLimit(String $bottom_limit) : AgeGroup
    {
        $this->bottom_limit = $bottom_limit;

        return $this;
    }

    public function getUpperLimit() 
    {
        return $this->upper_limit;
    }

    public function setUpperLimit(String $upper_limit) : AgeGroup
    {
        $this->upper_limit = $upper_limit;

        return $this;
    }
}
