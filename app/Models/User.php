<?php

namespace GTAChain\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

use GTAChain\Models\UserType;

use Exception;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    ####
    #   Relationship Definition Area
    ####

    public function type() 
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }

    ####
    #   Getters/Setters Area
    ####

    public function getId()
    {
        return $this->id;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName(String $name) : User
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail() 
    {
        return $this->email;
    }

    public function setEmail(String $email) : User
    {
        $this->email = $email;

        return $this;
    }

    public function getType() : UserType
    {
        return $this->type;
    }

    public function setType(UserType $type) : User
    {
        $this->type()->associate( $type );

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(String $password, String $password_confirmation) : User
    {
        if ($password !== $password_confirmation)
        {
            throw new Exception("Passwords didn't match!", 403);
        }

        $this->password = \Hash::make($password);

        return $this;
    }
}
