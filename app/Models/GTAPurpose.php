<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\GTA;

class GTAPurpose extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'gta_purposes';

    ####
    #   Relationship Definition Area
    ####

    public function GTAs() 
    {
        return $this->hasMany(GTA::class, 'gta_purpose_id');
    }

    ####
    #   Getters/Setters Area
    ####

    public function getGTAs()
    {
        return $this->GTAs()->get();
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName(String $name) : GTAPurpose
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym() 
    {
        return $this->acronym;
    }

    public function setAcronym(String $acronym) : GTAPurpose
    {
        $this->acronym = $acronym;

        return $this;
    }
}
