<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\AgeGroup;
use GTAChain\Models\Property;
use GTAChain\Models\GTA;

class Animal extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    // protected $table = "user_types";

    ####
    #   Relationship Definition Area
    ####

    public function age_group() 
    {
        return $this->belongsTo(AgeGroup::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function GTAs()
    {
        return $this->belongsToMany(GTA::class, 'animal_gta', 'animal_id', 'gta_id');
    }

    ####
    #   Getters/Setters Area
    ####

    public function getAgeGroup() : AgeGroup
    {
        return $this->age_group;
    }

    public function setAgeGroup(AgeGroup $group) : Animal
    {
        $this->age_group()->associate( $group );

        return $this;
    }

    public function getProperty() : Property
    {
        return $this->property;
    }

    public function setProperty(Property $property) : Animal
    {
        $this->property()->associate( $property );

        return $this;
    }

    public function getGTAs()
    {
        return $this->GTAs()->get();
    }

    public function getAmount() 
    {
        return $this->amount;
    }

    public function setAmount(String $amount) : Animal
    {
        $this->amount = $amount;

        return $this;
    }

    public function getGender() 
    {
        return $this->gender;
    }

    public function setGender(String $gender) : Animal
    {
        if( $gender != 'm' AND $gender != 'f' AND $gender != 'n/a' )
            throw new Exception('Invalid gender type for animal');

        $this->gender = $gender;

        return $this;
    }

    public function getSpecies() 
    {
        return $this->species;
    }

    public function setSpecies(String $species) : Animal
    {
        $this->species = $species;

        return $this;
    }

    public function getClassification() 
    {
        return $this->classification;
    }

    public function setClassification(String $classification) : Animal
    {
        $this->classification = $classification;

        return $this;
    }

    public function getRace() 
    {
        return $this->race;
    }

    public function setRace(String $race) : Animal
    {
        $this->race = $race;

        return $this;
    }
}
