<?php

namespace GTAChain\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt(String $format)
    {
        if( ! isset($this->created_at) )
            return null;

        if( $format != null )
            return \DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at)->format($format);
        else
            return $this->created_at;
    }

    public function getUpdatedAt(String $format)
    {
        if( ! isset($this->updated_at) )
            return null;

        if( $format != null )
            return \DateTime::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format($format);
        else
            return $this->updated_at;
    }
}
