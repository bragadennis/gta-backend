<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\Property;

class PropertyType extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    ####
    #   Relationship Definition Area
    ####

    public function properties() 
    {
        return $this->hasMany(Property::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getProperties() 
    {
        return $this->properties()->get();
    }
    
    public function getAcronym() 
    {
        return $this->acronym;
    }

    public function setAcronym(String $acronym) : PropertyType
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName(String $name) : PropertyType
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription() 
    {
        return $this->description;
    }

    public function setDescription(String $description) : PropertyType
    {
        $this->description = $description;

        return $this;
    }
}
