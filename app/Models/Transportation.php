<?php

namespace GTAChain\Models;

use Exception;

use GTAChain\Models\BaseModel;
use GTAChain\Models\GTA;

class Transportation extends BaseModel
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    // protected $table = "user_types";

    ####
    #   Relationship Definition Area
    ####

    public function GTAs() 
    {
        return $this->hasMany(GTA::class);
    }

    ####
    #   Getters/Setters Area
    ####

    public function getGTAs()
    {
        return $this->GTAs()->get();
    }

    public function getCompanyName() 
    {
        return $this->company_name;
    }

    public function setCompanyName(String $company_name) : Transportation
    {
        $this->company_name = $company_name;

        return $this;
    }

    public function getCompanyIdentification() 
    {
        return $this->company_identification;
    }

    public function setCompanyIdentification(String $company_identification) : Transportation
    {
        $this->company_identification = $company_identification;

        return $this;
    }

    public function getVehicleManufacturer() 
    {
        return $this->vehicle_manufacturer;
    }

    public function setVehicleManufacturer(String $vehicle_manufacturer) : Transportation
    {
        $this->vehicle_manufacturer = $vehicle_manufacturer;

        return $this;
    }

    public function getVehicleColor() 
    {
        return $this->vehicle_color;
    }

    public function setVehicleColor(String $vehicle_color) : Transportation
    {
        $this->vehicle_color = $vehicle_color;

        return $this;
    }

    public function getPlates() 
    {
        return $this->plates;
    }

    public function setPlates(String $plates) : Transportation
    {
        $this->plates = $plates;

        return $this;
    }
    
    public function getDriversName() 
    {
        return $this->drivers_name;
    }

    public function setDriversName(String $drivers_name) : Transportation
    {
        $this->drivers_name = $drivers_name;

        return $this;
    }

    public function getDriversLicenceNumber() 
    {
        return $this->drivers_licence_number;
    }

    public function setDriversLicenceNumber(String $drivers_licence_number) : Transportation
    {
        $this->drivers_licence_number = $drivers_licence_number;

        return $this;
    }
}
